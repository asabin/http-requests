import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Created by Arkady on 19.04.2018
 */

class UnirestExample {

    static String doRequest(String url, String json) throws UnirestException {
        HttpResponse<String> response = Unirest.post(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(json)
                .asString();
        return response.getStatusText();
    }
}
