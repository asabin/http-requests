import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

/**
 * Created by Arkady on 19.04.2018
 */

public class Main {

    public static void main(String[] args) {
//        -Djavax.net.debug=ssl

        String trustStore = "";
        String trustStorePassword = "";

        System.setProperty("javax.net.ssl.trustStore", trustStore);
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);

//        String trustStore = System.getProperties().getProperty("java.home")
//                + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts";

        try {
            showAllCertsInKeyStore(trustStore, trustStorePassword);
        } catch (Exception e) {
            e.printStackTrace();
        }

        doOkHttpRequest();

        System.out.println("---------------------------------");

        doUnirestRequest();

        System.out.println("---------------------------------");

        doStandardRequest();
    }

    private static void doOkHttpRequest() {
        try {
            System.out.println(OkHttpExample.doRequest(getUrl(), getJson()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void doUnirestRequest() {
        try {
            System.out.println(UnirestExample.doRequest(getUrl(), getJson()));
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    private static void doStandardRequest() {
        try {
            System.out.println(StandardExample.doRequest(getUrl(), getJson()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getUrl() {
        return "";
    }

    private static String getJson() {
        return "";
    }

    private static void showAllCertsInKeyStore(String keyStorePath, String keyStorePassword) throws Exception {
        FileInputStream is = new FileInputStream(keyStorePath);

        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        keystore.load(is, keyStorePassword.toCharArray());

        Enumeration e = keystore.aliases();
        for (; e.hasMoreElements(); ) {
            String alias = (String) e.nextElement();

            java.security.cert.Certificate cert = keystore.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                X509Certificate x509cert = (X509Certificate) cert;

                // Get subject
                Principal principal = x509cert.getSubjectDN();
                String subjectDn = principal.getName();
                System.out.println("subjectDn :" + subjectDn);

                // Get issuer
                principal = x509cert.getIssuerDN();
                String issuerDn = principal.getName();
                System.out.println("issuerDn : " + issuerDn);
            }
        }
    }
}
