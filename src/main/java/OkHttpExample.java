import okhttp3.*;

import java.io.IOException;

/**
 * Created by Arkady on 19.04.2018
 */

class OkHttpExample {

    static String doRequest(String url, String json) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();

        Response response = client.newCall(request).execute();
        return response.toString();
    }
}
